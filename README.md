pytelehambot
============

This is a Telegram bot written in Python that does ham radio call sign lookups via [QRZ.com](https://www.qrz.com/).


Things Needed
-------------

* a paid QRZ.com account with XML access, in order to use the QRZ.com lookup feature of this bot
* a token for a Telegram bot


LICENSE
-------

Copyright 2023, K8VSY, https://k8vsy.radio/
Licensed under the Eiffel Forum License 2

