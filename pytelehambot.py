#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright 2023, K8VSY, https://k8vsy.radio/
# Licensed under the Eiffel Forum License 2.


import datetime as dt
import flag
import logging
import maidenhead as mh
import numpy as np
import os
import pycountry
import re
import requests
from uuid import uuid4
import xmltodict

## python-telegram-bot imports
from telegram import Update, InlineQueryResultArticle, InputTextMessageContent
from telegram.ext import filters, MessageHandler, ApplicationBuilder, ContextTypes, CommandHandler, InlineQueryHandler
from telegram.constants import ParseMode


NOW = dt.datetime.now()
log_dt = NOW.strftime('%Y%m')

CWD = str(os.getcwd())

LOG_FILE = '{0}/logs/main-{1}.log'.format(CWD, str(log_dt))
logging.basicConfig(filename=LOG_FILE, level=logging.WARN,
                    format='%(asctime)s %(levelname)-8s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S')


def get_qrz_creds():
    '''
    Grab credentials for QRZ.com
    '''

    try:
        fh = open('qrz_creds.txt', 'r')
    except:
        print('Please create a `qrz_creds.txt` file in the same directory ' +
              'as this file in the format of: username,password')
        return None, None

    lines = list()

    if fh:
        lines = fh.readlines()

    fh.close()

    if len(lines) > 0:
        txt = lines[0]
        parts = txt.split(',')
        if len(parts) == 2:
            qrz_username = parts[0].strip()
            qrz_password = parts[1].strip()
        else:
            print('Not able to understand format of qrz_creds.txt file')
            return None, None
    else:
        return None, None

    return qrz_username, qrz_password


def get_telegram_token():
    '''
    Grab credentials for Telegram
    '''

    try:
        fh = open('telegram_token.txt', 'r')
    except:
        print('Pleaes create a `telegram_token.txt` file in the same ' +
              'directory as this file.')
        return None

    lines = list()

    if fh:
        lines = fh.readlines()

    fh.close()

    if len(lines) > 0:
        txt = lines[0]
        token_code = txt.strip()
        return token_code
    else:
        print('Not able to understand format of telegram_token.txt file')
        return None


def remove_weird_chars(txt):
    '''
    remove or escape characters as necessary for Telegram
    '''

    message = txt
    temp = message.replace('-', '\-')
    temp = temp.replace('.', '\.')
    temp = temp.replace('#', '\#')
    temp = temp.replace('(', '\(')
    temp = temp.replace('=', '\=')
    temp = temp.replace(')', '\)')
    temp = temp.replace('+', '\+')
    temp = temp.replace('!', '\!')
    return temp


def qrz_lookup(callsign):
    qrz_username, qrz_password = get_qrz_creds()
    if not qrz_username and not qrz_password:
        print('cannot log into QRZ')
        return {'error': 'cannot log into QRZ'}

    print('starting to hit QRZ.com')

    url = '''https://xmldata.qrz.com/xml/current/?username={0}&password={1}'''.format(qrz_username, qrz_password)
    session = requests.Session()
    r = session.get(url)
    raw_session = xmltodict.parse(r.content)
    session_key = raw_session.get('QRZDatabase').get('Session').get('Key')

    print('got session key from qrz.com')

    url2 = """https://xmldata.qrz.com/xml/current/?s={0}&callsign={1}""".format(session_key, callsign)
    r2 = session.get(url2)
    raw = xmltodict.parse(r2.content).get('QRZDatabase')
    ham = raw.get('Callsign')

    print('ham: ' + str(ham))
    return ham


def lookup_call(callsign):
    '''
    look up a given callsign via QRZ; and pretty-print the output
    '''

    print('input: ' + str(callsign)[:15])

    ## if call sign is too short or too long
    ## *and* if the first, second, or third character is *NOT* a number
    ## then it is NOT a valid call sign
    if len(callsign) < 3 or len(callsign) >= 15 or (not any([callsign[0].isnumeric(), callsign[1].isnumeric(), callsign[2].isnumeric()])):
            print('Not a valid call sign.')
            return False, 'Please provide a _valid_ call sign via, `/lookupviaQRZ \<callsign\>`'

    outs = qrz_lookup(str(callsign))
    print('outs: ' + str(outs))


    if not outs:
        print('No results found on QRZ.com')
        return False, 'No results found on QRZ\.com'

    elif 'error' in outs:
        print(outs['error'])
        return False, str(outs['error'])

    else:
        message = str()

        if 'country' in outs:
            try:
                country_emoji_search = pycountry.countries.search_fuzzy(outs['country'])

                if country_emoji_search:
                    ces_first = country_emoji_search[0]
                    if hasattr(ces_first, 'alpha_2'):
                        ces_format = ':{0}:'.format(ces_first.alpha_2)
                        ces_outs = flag.flagize(ces_format)
                        message += ces_outs
            except:
                message += str(outs['country'])

        if 'call' in outs:
            message += '  `{0}`'.format(str(outs['call']))

        message += '\n*Name*: '

        if 'fname' in outs:
            message += str(remove_weird_chars(outs['fname']))
        elif 'name' in outs:
            message += str(remove_weird_chars(outs['name']))


        if 'attn' in outs:
            message += '\n*Attention*: {0}'.format(str(remove_weird_chars(outs['attn'])))

        if 'aliases' in outs:
            message += '\n*Aliases*: {0}'.format(str(outs['aliases']))

        if 'trustee' in outs:
            message += '\n*Trustee*: {0}'.format(str(outs['trustee']))

        if 'class' in outs:
            message += '\n*Class*: {0}'.format(str(outs['class']))

        if 'addr2' in outs:
            message += '\n*Location*: {0}'.format(str(remove_weird_chars(outs['addr2'])))

        if 'state' in outs:
            message += ', {0}'.format(str(remove_weird_chars(outs['state'])))

        if 'country' in outs:
            message += ', _{0}_'.format(str(remove_weird_chars(outs['country'])))

        if 'grid' in outs:
            grid_code = str(outs['grid'][:4])
            # https://aprs.fi/#!addr=EM89
            message += ' \[[{0}]({1})\]'.format(grid_code, 'https://www.karhukoti.com/maidenhead-grid-square-locator/?grid=' + str(grid_code))

        if 'cqzone' in outs:
            message += '\n*CQ Zone*: {0}'.format(str(outs['cqzone']))

        if 'ituzone' in outs:
            message += '\n*ITU Zone*: {0}'.format(str(outs['ituzone']))

        if 'efdate' in outs and outs['efdate'] != '0000-00-00':
            message += '\n*Granted*: {0}'.format(str(remove_weird_chars(outs['efdate'])))

        if 'expdate' in outs and outs['expdate'] != '0000-00-00':
            message += '\n*Expiry*: {0}'.format(str(remove_weird_chars(outs['expdate'])))

        yes_var = '✅'
        no_var = '❌'

        if 'lotw' in outs:
            message += '\n*QSL via LoTW*: '
            if outs['lotw'] == '1':
                message += yes_var
            else:
                message += no_var

        if 'eqsl' in outs:
            message += '\n*QSL via eQSL*: '
            if outs['eqsl'] == '1':
                message += yes_var
            else:
                message += no_var

        if 'mqsl' in outs:
            message += '\n*QSL via Mail*: '
            if outs['mqsl'] == '1':
                message += yes_var
            else:
                message += no_var

        if 'qslmgr' in outs:
            message += '\n*QSL Manager*: {0}'.format(remove_weird_chars(str(outs['qslmgr'])))

        print('message: ' + str(message))

        message += '\n[Profile on QRZ](https://www.qrz.com/db/{0})'.format(str(outs['call']).upper())

        return True, message


def haversine_distance(lat1, lon1, lat2, lon2):
    r = 6371
    phi1 = np.radians(lat1)
    phi2 = np.radians(lat2)
    delta_phi = np.radians(lat2 - lat1)
    delta_lambda = np.radians(lon2 - lon1)
    a = np.sin(delta_phi / 2)**2 + np.cos(phi1) * np.cos(phi2) *   np.sin(delta_lambda / 2)**2
    res = r * (2 * np.arctan2(np.sqrt(a), np.sqrt(1 - a)))
    return np.round(res, 2)


async def distance(update, context):
    '''
    calculate the distance between two maidenhead locations
    '''

    error_mh = 'Please provide two valid Maidenhead locations.'

    if not context.args:
        await update.effective_message.reply_text('Please provide two valid maidenhead locations via, /distance <mh1> <mh2>', quote=True)
        return

    mh1 = re.escape(str(context.args[0]))
    mh2 = re.escape(str(context.args[1]))

    if len(mh1) < 4 or len(mh2) < 4 or len(mh1) > 6 or len(mh2) > 6 or not mh1[:2].isalpha() or not mh2[:2].isalpha():
        await update.effective_message.reply_text(text=error_mh, quote=True)
        return

    mh1_gps = mh.to_location(mh1)
    mh2_gps = mh.to_location(mh2)

    answer = haversine_distance(mh1_gps[0], mh1_gps[1], mh2_gps[0], mh2_gps[1])
    answer_miles = np.round(answer * 0.62137119224, 2)

    outs = 'Distance between {2} and {3} is: {0} km ({1} mi)'.format(str(answer), str(answer_miles), mh1.upper(), mh2.upper())

    print('outs: ' + str(outs))

    await update.effective_message.reply_text(text=outs, quote=True)


async def inline_call_lookup(update: Update, context: ContextTypes.DEFAULT_TYPE):
    '''
    Add ability to do inline queries to QRZ.com
    '''

    query = update.inline_query.query

    if not query:
        return

    callsign = re.escape(str(query).strip().upper())

    status, result = lookup_call(callsign)

    out_result = list()

    if status:
        out_result.append(
            InlineQueryResultArticle(
                id=callsign,
                title='QRZ.com result',
                input_message_content=InputTextMessageContent(result, parse_mode=ParseMode.MARKDOWN_V2,
                                                              disable_web_page_preview=True)
            )
        )

    await context.bot.answer_inline_query(update.inline_query.id, out_result)



async def normal_lookup_call(update, context):
    if not context.args:
        await update.effective_message.reply_text('Please provide a call sign via, /lookupviaQRZ <callsign>', quote=True)
        return

    callsign = str(context.args[0])

    callsign = re.escape(callsign)

    status, result = lookup_call(callsign)

    await update.effective_message.reply_text(text=result,
                                        parse_mode=ParseMode.MARKDOWN_V2,
                                        disable_web_page_preview=True,
                                        quote=True)


if __name__ == '__main__':

    token = get_telegram_token()
    application = ApplicationBuilder().token(token).build()

    lookup_handler = CommandHandler('lookupviaQRZ', normal_lookup_call)
    distance_handler = CommandHandler('distance', distance)
    inline_callsign_handler = InlineQueryHandler(inline_call_lookup)


    application.add_handler(distance_handler)
    application.add_handler(lookup_handler)
    application.add_handler(inline_callsign_handler)

    application.run_polling()

